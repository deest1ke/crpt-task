package ru.crpt.test;

import org.apache.commons.cli.ParseException;
import org.junit.Assert;
import org.junit.Test;
import ru.crpt.shell.CommandLineHelper;

/**
 * 
 * @author artyom
 */
public class CommandLineHelperTest
{
    @Test(expected = ParseException.class)
    public void testNoArgs() throws ParseException
    {
        new CommandLineHelper().parseArgs(null);
    }
    
    @Test(expected = ParseException.class)
    public void testEmptyArgs() throws ParseException
    {
        new CommandLineHelper().parseArgs(new String[] { });
    }        
    
    @Test(expected = ParseException.class)    
    public void testRequiredArgs() throws ParseException
    {
        CommandLineHelper helper = new CommandLineHelper();
        helper.parseArgs(new String[] { "-n" } );        
    }
    
    @Test
    public void testValidArgs() throws ParseException
    {        
        String val = "1";
        CommandLineHelper helper = new CommandLineHelper();
        helper.parseArgs(new String[] { "-n", val } );        
        Assert.assertEquals(val, helper.getNumber());
    }
}
