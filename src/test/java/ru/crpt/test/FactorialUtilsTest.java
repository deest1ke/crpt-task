package ru.crpt.test;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Test;
import ru.crpt.utils.FactorialUtils;

/**
 *
 * @author artyom
 */
public class FactorialUtilsTest
{       
    @Test(expected = NumberFormatException.class)
    public void testNegativeInteger()
    {
        FactorialUtils.trailingZeroes("-1");        
    }
    
    @Test(expected = NumberFormatException.class)
    public void testDouble()
    {
        FactorialUtils.trailingZeroes("10.1");
    }        
    
    @Test(expected = NumberFormatException.class)
    public void testString()
    {
        FactorialUtils.trailingZeroes("some string");
    }
    
    @Test
    public void testZero()
    {
        BigInteger num = FactorialUtils.trailingZeroes("0");
        Assert.assertEquals(num, BigInteger.ZERO);
    }
    
    @Test
    public void testPreparedValues()
    {
        Map<BigInteger, BigInteger> values = preparedValues();
        values.entrySet().forEach(entry ->
        {
            BigInteger expected = entry.getValue();
            BigInteger fact = FactorialUtils.trailingZeroes(entry.getKey());
            Assert.assertEquals(expected, fact);
        });
    }
        
    private static Map<BigInteger, BigInteger> preparedValues()
    {
        Map<BigInteger, BigInteger> values = new HashMap<>();
        
        values.put(BigInteger.ONE, BigInteger.ZERO);
        values.put(BigInteger.valueOf(2), BigInteger.ZERO);
        values.put(BigInteger.valueOf(3), BigInteger.ZERO);
        values.put(BigInteger.valueOf(4), BigInteger.ZERO);
        values.put(BigInteger.valueOf(5), BigInteger.ONE);
        values.put(BigInteger.valueOf(6), BigInteger.ONE);
        values.put(BigInteger.valueOf(7), BigInteger.ONE);
        values.put(BigInteger.valueOf(8), BigInteger.ONE);
        values.put(BigInteger.valueOf(9), BigInteger.ONE);
        
        BigInteger TWO = BigInteger.valueOf(2);
        
        values.put(BigInteger.valueOf(10), TWO);
        values.put(BigInteger.valueOf(11), TWO);
        values.put(BigInteger.valueOf(12), TWO);
        values.put(BigInteger.valueOf(13), TWO);
        values.put(BigInteger.valueOf(14), TWO);
        
        BigInteger THREE = BigInteger.valueOf(3);
        
        values.put(BigInteger.valueOf(15), THREE);
        values.put(BigInteger.valueOf(16), THREE);
        values.put(BigInteger.valueOf(17), THREE);
        values.put(BigInteger.valueOf(18), THREE);
        values.put(BigInteger.valueOf(19), THREE);
        
        BigInteger FOUR = BigInteger.valueOf(4);
        
        values.put(BigInteger.valueOf(20), FOUR);
        values.put(BigInteger.valueOf(21), FOUR);
        values.put(BigInteger.valueOf(22), FOUR);
        values.put(BigInteger.valueOf(23), FOUR);
        values.put(BigInteger.valueOf(24), FOUR);
        
        BigInteger SIX = BigInteger.valueOf(6);
        
        values.put(BigInteger.valueOf(25), SIX);
        values.put(BigInteger.valueOf(26), SIX);
        values.put(BigInteger.valueOf(27), SIX);
        values.put(BigInteger.valueOf(28), SIX);
        
        values.put(BigInteger.valueOf(1000), BigInteger.valueOf(249));
        
        return values;
    }
}
