package ru.crpt.test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.crpt.Run;

/**
 *
 * @author artyom
 */
public class RunTest
{   
    @Test
    @Ignore
    public void testApplication()
    {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        Run.main(new String[] { "-n", "5" });
        System.setOut(System.out);
        
        // Depends on logging level
        Assert.assertEquals("1", outContent.toString().trim());
    }
}
