package ru.crpt.utils;

import java.math.BigInteger;

/**
 * This class defines utility-methods to find the number of trailing zeros in the factorial.
 * @author artyom
 */
public class FactorialUtils
{
    private static BigInteger FIVE = BigInteger.valueOf(5L);
    
    /**
     * Finds the number of trailing zeros in the factorial of the {@code number}.
     * @param number {@link java.lang.String} representation of number.
     * @return the number of trailing zeros in the factorial of the {@code number}.
     * @throws NumberFormatException {@code number} is not a valid representation
     *         of a BigInteger.          
     */
    public static BigInteger trailingZeroes(String number) throws NumberFormatException
    {
        return trailingZeroes(new BigInteger(number));        
    }
    
    /**
     * Finds the number of trailing zeros in the factorial of the {@code number}.
     * @param number {@link java.math.BigInteger} representation of number.
     * @return the number of trailing zeros in the factorial of the {@code number}.
     * @throws NumberFormatException {@code val} is negative number.
     */
    public static BigInteger trailingZeroes(BigInteger number) throws NumberFormatException
    {
        if (number.compareTo(BigInteger.ZERO) < 0)
        {
            throw new NumberFormatException("The number is negative!");
        }

        BigInteger count = BigInteger.ZERO;
        for (BigInteger i = FIVE; number.divide(i).compareTo(BigInteger.ONE) >= 0; i = i.multiply(FIVE)) 
        {
            count = count.add(number.divide(i));
        }

        return count;
    }
}
