package ru.crpt;

import java.math.BigInteger;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.crpt.shell.CommandLineHelper;
import ru.crpt.utils.FactorialUtils;

/**
 * This class represents an entry-point of application.
 * @author artyom
 */
public class Run
{
    private static final Logger log = LoggerFactory.getLogger(Run.class);
    
    public static void main(String[] args)
    {                
        String number = parseNumber(args);
        try
        {
            BigInteger bigInteger = FactorialUtils.trailingZeroes(number);
            System.out.println(bigInteger);
        }
        catch (NumberFormatException ex)
        {
            log.error("There is no correct non-negative integer: {}", number, ex);
            System.out.println("Be sure that you input correct non-negative integer");            
        }       
        catch (Exception ex)
        {
            log.error("Unexpected exception occurred for input {}", number, ex);            
            System.err.println("Unexpected error, please see log for more details");
        }
    }   
    
    private static String parseNumber(String args[])
    {        
        CommandLineHelper commandLineHelper = new CommandLineHelper();
        try 
        {            
            commandLineHelper.parseArgs(args);
            return commandLineHelper.getNumber();
        }
        catch (ParseException ex)
        {            
            commandLineHelper.printPrettyHelp();
         
            // Don't log it with ERROR or WARN levels, the help message will be shown
            log.debug("Error occurred while parsing arguments", ex);
            
            System.exit(0);            
            return null;
        }
    }
}