package ru.crpt.shell;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * This is a simple class to read and to parse the command 
 * line arguments based on prepared options.
 * 
 * @author artyom
 */
public class CommandLineHelper
{               
    /**
     * Options names
     */
    private static final String OPTION_NUMBER   = "n";
    private static final String OPTION_HELP     = "h";
    
    /**
     * Help messages
     */
    private static final String HELP_HEADER = "Finds the number of trailing zeros in the factorial of the number";
    private static final String HELP_FOOTER = "Please report issues at deest1ke@yandex.ru";
    
    private final Options options = new Options();
    private final HelpFormatter formatter = new HelpFormatter();
    
    private CommandLine commandLine;          
    
    public CommandLineHelper()
    {        
        initOptions();        
    }
    
    /**
     * Parse the arguments according to the application options.
     * 
     * @param args the command line arguments
     * @throws ParseException if there are any problems encountered
     * while parsing the command line tokens.
     */
    public void parseArgs(String[] args) throws ParseException
    {
        CommandLineParser parser = new DefaultParser();
        commandLine = parser.parse(options, args);               
    }
     
    /**
     * Print the help for this application with the specified
     * command line syntax. 
     * This method prints help information to System.out.
     */
    public void printPrettyHelp()
    {
        formatter.printHelp("java -jar application.jar", HELP_HEADER, options, HELP_FOOTER, true);
    }
    
    /**
     * Returns the value of the number argument.
     * @return the value of the number argument.
     */
    public String getNumber()
    {
        return commandLine.getOptionValue(OPTION_NUMBER);
    }
    
    private void initOptions()
    {
        options.addOption(Option.builder(OPTION_NUMBER)
                        .longOpt("number")
                        .required(true)
                        .desc("number for which calculations are performed")
                        .hasArg(true)        
                        .argName("num")
                        .build());   
        
        options.addOption(Option.builder(OPTION_HELP)
                        .longOpt("help")
                        .desc("display this help and exit")
                        .build());
    }        
}