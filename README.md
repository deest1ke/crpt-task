# Factorial Trailing Zeros
> Finds the number of trailing zeros in the factorial of the number

## Manual installation

Maven

```sh
mvn clean install -Puberjar 
```

## Usage

```sh
usage: java -jar application.jar [-h] -n <num>
Finds the number of trailing zeros in the factorial of the number
 -h,--help           display this help and exit
 -n,--number <num>   number for which calculations are performed
Please report issues at deest1ke@yandex.ru
```
## Usage example

```sh
java -jar application.jar -n 1000 
249
```

